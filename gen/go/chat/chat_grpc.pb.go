// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.20.3
// source: chat/chat.proto

package chatv1

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// ChatClient is the client API for Chat service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type ChatClient interface {
	Connect(ctx context.Context, opts ...grpc.CallOption) (Chat_ConnectClient, error)
	UserDialogs(ctx context.Context, in *UserDialogsRequest, opts ...grpc.CallOption) (*UserDialogsResponse, error)
	Users(ctx context.Context, in *UsersRequest, opts ...grpc.CallOption) (*UsersSmall, error)
	User(ctx context.Context, in *UserRequest, opts ...grpc.CallOption) (*UserSmall, error)
	CreateDiscussion(ctx context.Context, in *CreateDiscussionRequest, opts ...grpc.CallOption) (*CreateDiscussionResponse, error)
	SearchUser(ctx context.Context, in *SearchUserRequest, opts ...grpc.CallOption) (*UsersSmall, error)
}

type chatClient struct {
	cc grpc.ClientConnInterface
}

func NewChatClient(cc grpc.ClientConnInterface) ChatClient {
	return &chatClient{cc}
}

func (c *chatClient) Connect(ctx context.Context, opts ...grpc.CallOption) (Chat_ConnectClient, error) {
	stream, err := c.cc.NewStream(ctx, &Chat_ServiceDesc.Streams[0], "/chat.chat/Connect", opts...)
	if err != nil {
		return nil, err
	}
	x := &chatConnectClient{stream}
	return x, nil
}

type Chat_ConnectClient interface {
	Send(*Message) error
	Recv() (*MessageWithUser, error)
	grpc.ClientStream
}

type chatConnectClient struct {
	grpc.ClientStream
}

func (x *chatConnectClient) Send(m *Message) error {
	return x.ClientStream.SendMsg(m)
}

func (x *chatConnectClient) Recv() (*MessageWithUser, error) {
	m := new(MessageWithUser)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *chatClient) UserDialogs(ctx context.Context, in *UserDialogsRequest, opts ...grpc.CallOption) (*UserDialogsResponse, error) {
	out := new(UserDialogsResponse)
	err := c.cc.Invoke(ctx, "/chat.chat/UserDialogs", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *chatClient) Users(ctx context.Context, in *UsersRequest, opts ...grpc.CallOption) (*UsersSmall, error) {
	out := new(UsersSmall)
	err := c.cc.Invoke(ctx, "/chat.chat/Users", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *chatClient) User(ctx context.Context, in *UserRequest, opts ...grpc.CallOption) (*UserSmall, error) {
	out := new(UserSmall)
	err := c.cc.Invoke(ctx, "/chat.chat/User", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *chatClient) CreateDiscussion(ctx context.Context, in *CreateDiscussionRequest, opts ...grpc.CallOption) (*CreateDiscussionResponse, error) {
	out := new(CreateDiscussionResponse)
	err := c.cc.Invoke(ctx, "/chat.chat/CreateDiscussion", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *chatClient) SearchUser(ctx context.Context, in *SearchUserRequest, opts ...grpc.CallOption) (*UsersSmall, error) {
	out := new(UsersSmall)
	err := c.cc.Invoke(ctx, "/chat.chat/SearchUser", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ChatServer is the server API for Chat service.
// All implementations must embed UnimplementedChatServer
// for forward compatibility
type ChatServer interface {
	Connect(Chat_ConnectServer) error
	UserDialogs(context.Context, *UserDialogsRequest) (*UserDialogsResponse, error)
	Users(context.Context, *UsersRequest) (*UsersSmall, error)
	User(context.Context, *UserRequest) (*UserSmall, error)
	CreateDiscussion(context.Context, *CreateDiscussionRequest) (*CreateDiscussionResponse, error)
	SearchUser(context.Context, *SearchUserRequest) (*UsersSmall, error)
	mustEmbedUnimplementedChatServer()
}

// UnimplementedChatServer must be embedded to have forward compatible implementations.
type UnimplementedChatServer struct {
}

func (UnimplementedChatServer) Connect(Chat_ConnectServer) error {
	return status.Errorf(codes.Unimplemented, "method Connect not implemented")
}
func (UnimplementedChatServer) UserDialogs(context.Context, *UserDialogsRequest) (*UserDialogsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UserDialogs not implemented")
}
func (UnimplementedChatServer) Users(context.Context, *UsersRequest) (*UsersSmall, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Users not implemented")
}
func (UnimplementedChatServer) User(context.Context, *UserRequest) (*UserSmall, error) {
	return nil, status.Errorf(codes.Unimplemented, "method User not implemented")
}
func (UnimplementedChatServer) CreateDiscussion(context.Context, *CreateDiscussionRequest) (*CreateDiscussionResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateDiscussion not implemented")
}
func (UnimplementedChatServer) SearchUser(context.Context, *SearchUserRequest) (*UsersSmall, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SearchUser not implemented")
}
func (UnimplementedChatServer) mustEmbedUnimplementedChatServer() {}

// UnsafeChatServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to ChatServer will
// result in compilation errors.
type UnsafeChatServer interface {
	mustEmbedUnimplementedChatServer()
}

func RegisterChatServer(s grpc.ServiceRegistrar, srv ChatServer) {
	s.RegisterService(&Chat_ServiceDesc, srv)
}

func _Chat_Connect_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(ChatServer).Connect(&chatConnectServer{stream})
}

type Chat_ConnectServer interface {
	Send(*MessageWithUser) error
	Recv() (*Message, error)
	grpc.ServerStream
}

type chatConnectServer struct {
	grpc.ServerStream
}

func (x *chatConnectServer) Send(m *MessageWithUser) error {
	return x.ServerStream.SendMsg(m)
}

func (x *chatConnectServer) Recv() (*Message, error) {
	m := new(Message)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func _Chat_UserDialogs_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UserDialogsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ChatServer).UserDialogs(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/chat.chat/UserDialogs",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ChatServer).UserDialogs(ctx, req.(*UserDialogsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Chat_Users_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UsersRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ChatServer).Users(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/chat.chat/Users",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ChatServer).Users(ctx, req.(*UsersRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Chat_User_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UserRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ChatServer).User(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/chat.chat/User",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ChatServer).User(ctx, req.(*UserRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Chat_CreateDiscussion_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateDiscussionRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ChatServer).CreateDiscussion(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/chat.chat/CreateDiscussion",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ChatServer).CreateDiscussion(ctx, req.(*CreateDiscussionRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Chat_SearchUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SearchUserRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ChatServer).SearchUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/chat.chat/SearchUser",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ChatServer).SearchUser(ctx, req.(*SearchUserRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// Chat_ServiceDesc is the grpc.ServiceDesc for Chat service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var Chat_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "chat.chat",
	HandlerType: (*ChatServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "UserDialogs",
			Handler:    _Chat_UserDialogs_Handler,
		},
		{
			MethodName: "Users",
			Handler:    _Chat_Users_Handler,
		},
		{
			MethodName: "User",
			Handler:    _Chat_User_Handler,
		},
		{
			MethodName: "CreateDiscussion",
			Handler:    _Chat_CreateDiscussion_Handler,
		},
		{
			MethodName: "SearchUser",
			Handler:    _Chat_SearchUser_Handler,
		},
	},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "Connect",
			Handler:       _Chat_Connect_Handler,
			ServerStreams: true,
			ClientStreams: true,
		},
	},
	Metadata: "chat/chat.proto",
}
